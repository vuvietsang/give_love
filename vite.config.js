import reactRefresh from "@vitejs/plugin-react-refresh";
import { resolve } from "path";
import eslint from "@rbnlffl/rollup-plugin-eslint";
import alias from "./src/config/aliases";
// import { VitePWA } from "vite-plugin-pwa";
// import eslintPlugin from "vite-plugin-eslint";
// https://vitejs.dev/config/
const aliases = alias();
const resolvedAliases = Object.fromEntries(
  Object.entries(aliases).map(([key, value]) => [
    key,
    resolve(__dirname, value),
  ])
);
const config = ({ command }) => ({
  plugins: [
    reactRefresh(),
    eslint({
      extensions: ["js", "jsx", "ts", "tsx"],
      filterInclude: ["**/*.{js,ts,jsx,tsx}"],
      throwOnError: true,
    }),
    // VitePWA(),
  ],
  // base: command === "serve" ? "" : "",
  // define: {
  //   global: "window",
  // },
  resolve: {
    alias: resolvedAliases,
  },
  define: {
    "process.env": {},
  },
  server: {
    fs: {
      // Allow serving files from one level up to the project root
      allow: [".."],
      strict: false,
    },
  },
});
export default config;
