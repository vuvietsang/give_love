export interface ICampaign {
  id: number;
  startDate: string;
  endDate: string;
  location: string;
  total: number;
  totalExpenses: number;
  totalMember: number;
  description: string;
  name: string;
  state: string;
  manager: string;
  image: string;
}
