import { useQuery } from "react-query";
import { getAccountById } from "../../../services/account";

const useAccountById = (id: string) => {
  return useQuery(["account"], async () => {
    return await getAccountById(id);
  });
};

export default useAccountById;
