import { useMutation, useQueryClient } from "react-query";
import { UpdateAccountDto } from "src/feature/services/account/dto/update-account-dto";
import { updatePassword } from "../../../services/account";

const useUpdatePassword = () => {
  const queryClient = useQueryClient();
  let key = "";
  return useMutation(
    async (data: UpdateAccountDto) => {
      key = data.userId;
      return await updatePassword(data);
    }
    // {
    //   onSettled: () => {
    //     queryClient.invalidateQueries("accounts");
    //     queryClient.invalidateQueries(`account${key}`);
    //   },
    // }
  );
};

export default useUpdatePassword;
