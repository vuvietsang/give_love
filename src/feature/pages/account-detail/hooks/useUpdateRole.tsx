import { useMutation, useQueryClient } from "react-query";
import { UpdateAccountDto } from "src/feature/services/account/dto/update-account-dto";
import { updateRole } from "../../../services/account";

const useUpdateRole = () => {
  const queryClient = useQueryClient();
  let key = "";
  return useMutation(
    async (data: UpdateAccountDto) => {
      key = data.userId;
      return await updateRole(data);
    },
    {
      onSettled: () => {
        queryClient.invalidateQueries("accounts");
        queryClient.invalidateQueries(`account${key}`);
      },
    }
  );
};

export default useUpdateRole;
