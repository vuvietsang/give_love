import Button from "@atlaskit/button/loading-button";
import React, { useState } from "react";
import { useParams } from "react-router";
import { useHistory } from "react-router-dom";

interface RegisForm {
  closeModal: () => void;
}

const SuccessNotificationSection: React.FC<RegisForm> = (props) => {
  const history = useHistory();
  const { id } = useParams<{ id: string }>();

  const { closeModal } = props;
  const close = () => {
    closeModal();
  };

  return (
    <div className="mb-10">
      <Button appearance="primary" onClick={() => close()}>
        Đồng ý
      </Button>
    </div>
  );
};

export default SuccessNotificationSection;
