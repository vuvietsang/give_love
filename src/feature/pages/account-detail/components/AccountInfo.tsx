import Button from "@atlaskit/button/loading-button";
import Form, { FormFooter } from "@atlaskit/form";
import SelectField from "@components/formInputs/SelectField";
import TextView from "@components/formInputs/TextView";
import React, { useEffect } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { useParams } from "react-router";
import useAccount from "../hooks/useAccount";
import useUpdateRole from "../hooks/useUpdateRole";
interface FormValues {
  username: string;
  email: string;
  roleSelect: { label: string; value: string };
  role: string;
}
const AccountInfo = ({
  edit = false,
  setEdit,
}: {
  edit?: boolean;
  setEdit: (data: boolean) => void;
}) => {
  const { mutate: updateRole, isLoading: updateLoading } = useUpdateRole();

  const { id } = useParams<{ id: string }>();
  const { data, isLoading } = useAccount(id);
  const selectionData = [
    { key: "Quản lí chiến dịch", value: "2" },
    { key: "Thành viên", value: "3" },
    { key: "Mạnh thường quân", value: "4" },
  ];
  const roleMap = new Map();
  roleMap.set("Manager", "Quản lí chiến dịch");
  roleMap.set("Member", "Thành viên");
  roleMap.set("Donator", "Mạnh thường quân");

  // const schema = yup.object().shape({
  //   username: yup
  //     .string()
  //     .typeError("Không đúng định dạng")
  //     .required("Tên đăng nhập không được để trống")
  //     .min(3, "Cần ít nhất 3 ký tự"),
  //   email: yup
  //     .string()
  //     .typeError("Không đúng định dạng")
  //     .required("Email không được để trống")
  //     .email("Không đúng định dạng"),
  // });

  const RHFformProps = useForm<FormValues>({
    defaultValues: {
      roleSelect: { label: "Thành viên", value: "1" },
    },
    // resolver: yupResolver(schema),
  });
  const { handleSubmit } = RHFformProps;
  useEffect(() => {
    if (isLoading) return;
    RHFformProps.reset({
      roleSelect: { label: "Thành viên", value: "1" },
    });

    return () => {};
  }, [data, isLoading, RHFformProps]);
  const onCancel = () => {
    setEdit(false);
    RHFformProps.reset({
      roleSelect: { label: "Thành viên", value: "1" },
    });
    RHFformProps.clearErrors();
  };
  return (
    <>
      <Form onSubmit={() => {}} isDisabled={!edit || updateLoading}>
        {({ formProps, dirty, submitting }) => (
          <FormProvider {...RHFformProps}>
            <form
              {...formProps}
              onSubmit={handleSubmit((data) => {
                const requestData = { ...data, role: data.roleSelect.value };
                updateRole(
                  { ...requestData, userId: id },
                  {
                    onSuccess: () => {
                      setEdit(false);
                    },
                  }
                );
              })}
            >
              <div className="grid grid-cols-2 gap-x-4">
                <>
                  <TextView
                    label="Tên đăng nhập"
                    name="username"
                    disabled
                    value={!data ? "" : data.data[0].username}
                  />
                  <TextView
                    label="Họ và tên"
                    name="fullName"
                    disabled
                    value={!data ? "" : data.data[0].fullName}
                  />
                  <TextView
                    label="Số điện thoại"
                    name="phone"
                    disabled
                    value={!data ? "" : data.data[0].phone}
                  />

                  <TextView
                    label="Email"
                    name="email"
                    disabled
                    value={!data ? "" : data.data[0].email}
                  />
                  {edit ? (
                    <SelectField
                      data={selectionData}
                      form={RHFformProps}
                      label="Vai trò"
                      name="roleSelect"
                    />
                  ) : (
                    <TextView
                      label="Vai trò"
                      name="role"
                      disabled
                      value={!data ? "" : roleMap.get(data.data[0].role)}
                    />
                  )}
                  <TextView
                    label="Địa chỉ"
                    name="address"
                    disabled
                    value={!data ? "" : data.data[0].address}
                  />
                </>
              </div>

              {edit && (
                <FormFooter>
                  <div className="space-x-2">
                    <Button
                      type="button"
                      appearance="danger"
                      isLoading={updateLoading}
                      onClick={onCancel}
                    >
                      Hủy
                    </Button>
                    <Button
                      type="submit"
                      appearance="primary"
                      isLoading={updateLoading}
                    >
                      Đồng ý
                    </Button>
                  </div>
                </FormFooter>
              )}
            </form>
          </FormProvider>
        )}
      </Form>
    </>
  );
};

export default AccountInfo;
