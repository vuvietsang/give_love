import Button from "@atlaskit/button";
import React, { useCallback, useState } from "react";
import { ModalTransition } from "@atlaskit/modal-dialog";
import Modal, {
  ModalBody,
  ModalHeader,
  ModalTitle,
} from "@atlaskit/modal-dialog";
import UpdatePasswordForm from "./UpdatePasswordForm";
import SuccessNotificationSection from "./SuccessNotificationSection";
const UpdatePasswordSection = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isOpenConfirm, setIsOpenConfirm] = useState(false);
  const openModal = useCallback(() => setIsOpen(true), []);
  const closeModal = useCallback(() => setIsOpen(false), []);
  const openModalConfirm = useCallback(() => setIsOpenConfirm(true), []);
  const closeModalConfirm = useCallback(() => setIsOpenConfirm(false), []);
  return (
    <>
      <Button appearance="primary" onClick={openModal}>
        {"Đổi mật khẩu"}
      </Button>
      <ModalTransition>
        {isOpen && (
          <Modal scrollBehavior="outside">
            <ModalHeader>
              <ModalTitle> {"Cập nhật mật khẩu"}</ModalTitle>
            </ModalHeader>
            <ModalBody>
              <UpdatePasswordForm
                closeModal={closeModal}
                openModalConfirm={openModalConfirm}
              />
            </ModalBody>
          </Modal>
        )}
        {isOpenConfirm && (
          <Modal scrollBehavior="outside">
            <ModalHeader>
              <ModalTitle> {"Cập nhật thành công"}</ModalTitle>
            </ModalHeader>
            <ModalBody>
              <SuccessNotificationSection closeModal={closeModalConfirm} />
            </ModalBody>
          </Modal>
        )}
      </ModalTransition>
    </>
  );
};

export default UpdatePasswordSection;
