import React, { SyntheticEvent } from "react";
import Pagination from "@atlaskit/pagination";
import { UIAnalyticsEvent } from "@atlaskit/analytics-next";

const PaginationFeature = ({
  page,
  setPage,
  totalPage = 1,
}: {
  totalPage?: number;
  page: number;
  setPage: (data: number) => void;
}) => {
  const pageNumList = [];
  for (let index = 1; index <= totalPage; index++) {
    pageNumList.push(index);
  }
  // console.log(pageNumList);
  return totalPage ? (
    <Pagination
      defaultSelectedIndex={1}
      pages={pageNumList}
      onChange={(
        event: SyntheticEvent<Element, Event>,
        page: number,
        analyticsEvent?: UIAnalyticsEvent | undefined
      ) => {
        setPage(page - 1);
        console.log(page);
      }}
      selectedIndex={page}
    />
  ) : null;
};

export default PaginationFeature;
