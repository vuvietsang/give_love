import Spiner from "@atlaskit/spinner";
import React, { useState } from "react";
import { useParams } from "react-router";
import useExpenses from "../hooks/useExpenses";
import "../styles.css";
import ExpenseTable from "./ExpenseTable";
import PaginationFeature from "./PaginationFeature";

const ListExpense = () => {
  const [page, setPage] = useState(0);
  const { id } = useParams<{ id: string }>();

  const { data, isLoading } = useExpenses(id, { pageNum: page });
  // @ts-ignore
  if (!data && !data?.data) {
    return <Spiner />;
  }

  return (
    <>
      <ExpenseTable data={data} isLoading={isLoading} />
      {/* <div className="flex justify-center">
        <PaginationFeature
          page={page}
          setPage={setPage}
          totalPage={data?.totalPage}
        />
      </div> */}
    </>
  );
};

export default ListExpense;
