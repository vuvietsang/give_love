import { useQuery } from "react-query";
import { getExpense } from "../../../services/expense";
interface Filter {
  pageSize?: number;
  pageNum?: number;
  state?: number;
  startDate?: Date;
  endDate?: Date;
  name?: string;
  sort?: string;
}
const useExpenses = (campaignId: string, filter?: Filter) => {
  const { pageNum = 0 } = filter || {};
  return useQuery(["expense", pageNum], async () => {
    return await getExpense(campaignId, filter);
  });
};

export default useExpenses;
