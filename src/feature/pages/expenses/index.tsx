import PageHeader from "@atlaskit/page-header";
import React from "react";
import ListExpense from "./components/ListExpense";
import { useTranslation } from "react-i18next";
import ExportExcelSection from "./components/ExportExcelSection";
import { useParams } from "react-router";
import useCampaignDetail from "../campaign-detail/hooks/useCampaignDetail";
import useExpenses from "./hooks/useExpenses";

const Expenses = () => {
  return (
    <div>
      <PageHeader actions={<ExportExcelSection />}>
        Danh sách các khoản chi
      </PageHeader>

      <ListExpense />
    </div>
  );
};

export default Expenses;
