import { useQuery } from "react-query";

const useAccountProfile = () => {
  return useQuery(["accountProfile"], async () => {
    // return await getCompanyProfile();
  });
};

export default useAccountProfile;
