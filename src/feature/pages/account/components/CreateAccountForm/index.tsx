import Button from "@atlaskit/button/loading-button";
import Form, { FormFooter } from "@atlaskit/form";
import InputField from "@components/formInputs/InputField";
import PasswordField from "@components/formInputs/PasswordField";
import SelectField from "@components/formInputs/SelectField";
import NetworkError from "@components/NetworkError";
import { yupResolver } from "@hookform/resolvers/yup";
import { AxiosError } from "axios";
import React from "react";
import { FormProvider, useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";
import { CreateAccountDto } from "src/feature/services/account/dto/create-account-dto";
import { ErrorHttpResponse } from "src/interfaces/error_http_response.interface";
import useCreateAccount from "../../hooks/useCreateAccount";
import useAccountForm, { User } from "./useAccountForm";

interface RegisForm {
  closeModal: () => void;
}

const CreateAccountForm: React.FC<RegisForm> = (props) => {
  const history = useHistory();
  const { defaultValues, schema } = useAccountForm();
  const { mutate: createAccount, isLoading, error } = useCreateAccount();
  const { closeModal } = props;
  const selectionData = [
    { key: "Quản lí chiến dịch", value: "2" },
    { key: "Thành viên", value: "3" },
    { key: "Mạnh thường quân", value: "4" },
  ];
  const close = () => {
    closeModal();
  };

  const form = useForm<User>({
    defaultValues,
    resolver: yupResolver(schema),
  });

  const { handleSubmit, ...formFunc } = form;

  return (
    <Form
      onSubmit={(data) => {
        console.log("form data", data);
      }}
      isDisabled={isLoading}
    >
      {({ formProps, dirty, submitting }) => (
        <form
          {...formProps}
          onSubmit={handleSubmit((data: User) => {
            // history.push("/agency-details?edit=1");
            const submitData: CreateAccountDto = {
              ...data,
              roleId: data.role.value,
            };

            createAccount(
              { ...submitData },
              {
                onSuccess: (data) => {
                  history.push(`/account-details/${data?.data?.id}`);
                },
              }
            );
          })}
        >
          <div className="">
            <FormProvider {...formFunc} handleSubmit={handleSubmit}>
              <div>
                <InputField form={form} label="Tên đăng nhập" name="username" />
                <PasswordField form={form} label="Mật khẩu" name="password" />
                <InputField form={form} label="Email" name="email" />
                <InputField form={form} label="Số điện thoại" name="phone" />
                <SelectField
                  data={selectionData}
                  form={form}
                  label="Vai trò"
                  name="role"
                />
              </div>
            </FormProvider>
          </div>
          {/* <NetworkError error={error as any} /> */}
          <FormFooter>
            <div className="flex justify-center w-full pb-10">
              <div className="w-full mr-5">
                <Button
                  appearance="subtle"
                  shouldFitContainer
                  onClick={close}
                  isDisabled={isLoading}
                >
                  {"Hủy"}
                </Button>
              </div>
              <div className="w-full">
                <Button
                  type="submit"
                  shouldFitContainer
                  appearance="primary"
                  isLoading={isLoading}
                >
                  {"Khởi tạo"}
                </Button>
              </div>
            </div>
          </FormFooter>
        </form>
      )}
    </Form>
  );
};

export default CreateAccountForm;
