import Button from "@atlaskit/button";
import React, { useCallback, useState } from "react";
import { ModalTransition } from "@atlaskit/modal-dialog";
import Modal, {
  ModalBody,
  ModalHeader,
  ModalTitle,
} from "@atlaskit/modal-dialog";
import CreateAccountForm from "./CreateAccountForm";
const CreateDepartmentSection = () => {
  const [isOpen, setIsOpen] = useState(false);
  const openModal = useCallback(() => setIsOpen(true), []);
  const closeModal = useCallback(() => setIsOpen(false), []);
  return (
    <>
      <Button appearance="primary" onClick={openModal}>
        {"Tạo tài khoản"}
      </Button>
      <ModalTransition>
        {isOpen && (
          <Modal scrollBehavior="outside">
            <ModalHeader>
              <ModalTitle> {"Nhập thông tin"}</ModalTitle>
            </ModalHeader>
            <ModalBody>
              <CreateAccountForm closeModal={closeModal} />
            </ModalBody>
          </Modal>
        )}
      </ModalTransition>
    </>
  );
};

export default CreateDepartmentSection;
