import Spiner from "@atlaskit/spinner";
import { useAppSelector } from "@components/hooks/reduxHook";
import React, { useState } from "react";
import { shallowEqual } from "react-redux";
import { useParams } from "react-router";
import AccountTable from "../../account/components/AccountTable";
import useMemberInCampaign from "../hooks/useMemberInCampaign";
import "../styles.css";
import MemberInCampaignTable from "./MemberInCampaignTable";

const ListMemberInCampaign = () => {
  const { id } = useParams<{ id: string }>();
  const { data, isLoading } = useMemberInCampaign(id);
  const role = useAppSelector((state) => state.auth?.role, shallowEqual);
  // @ts-ignore
  if (!data) {
    return <Spiner />;
  }
  let realData = data;
  realData = data.filter((x) => {
    if (x.role === "Member") return x;
  });

  return (
    <>
      <MemberInCampaignTable
        data={realData}
        isLoading={isLoading}
        role={role}
      />
    </>
  );
};

export default ListMemberInCampaign;
