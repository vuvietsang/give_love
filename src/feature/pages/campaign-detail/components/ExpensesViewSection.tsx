import Button from "@atlaskit/button";
import React from "react";

import { useTranslation } from "react-i18next";
import { useHistory } from "react-router";
const ExpensesViewSection = ({ userId }: { userId: string }) => {
  const history = useHistory();
  return (
    <>
      <div>
        <div className="space-x-2">
          <Button
            onClick={() => {
              history.goBack();
            }}
          >
            Trở lại
          </Button>
          <Button
            appearance="primary"
            onClick={() => history.push(`/campaign-expense/${userId}`)}
          >
            Xem danh sách chi
          </Button>
        </div>
      </div>
    </>
  );
};

export default ExpensesViewSection;
