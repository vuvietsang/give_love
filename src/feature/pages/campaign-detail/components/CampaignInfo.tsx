import Spiner from "@atlaskit/spinner";
import Textfield from "@atlaskit/textfield";
import TextAreaView from "@components/formInputs/TextAreaView";
import TextView from "@components/formInputs/TextView";
import React from "react";
import { useHistory, useParams } from "react-router";
import useCampaignDetail from "../hooks/useCampaignDetail";
import Button from "@atlaskit/button";
import { numberWithCommas } from "../../../../helpers/index";
import { useAppSelector } from "@components/hooks/reduxHook";
import { shallowEqual } from "react-redux";
import styled from "@emotion/styled";
interface FormValues {
  username: string;
  email: string;
  role: string;
}
const CampaignInfo = () => {
  const { id } = useParams<{ id: string }>();
  const { data, isLoading } = useCampaignDetail(id);
  const role = useAppSelector((state) => state.auth?.role, shallowEqual);
  const history = useHistory();

  let CustomButton = styled(Button)`
    height: 40px;
    padding-top: 4px;
    width: 25%;
  `;

  return isLoading ? (
    <Spiner />
  ) : (
    <div className="">
      <div>
        <div className="grid grid-cols-2 gap-x-12">
          {role === "Admin" && (
            <TextView label="ID" name="id" value={data?.data[0].id + ""} />
          )}
          <TextView
            label="Tên chiến dịch"
            name="Name"
            value={data?.data[0].name + ""}
          />
          {role === "Donator" && (
            <div className="flex items-end">
              <TextView
                label="Số lượng thành viên"
                name="total member"
                value={data?.data[0].totalMember + ""}
              />
              <CustomButton
                appearance="primary"
                onClick={() => history.push(`/members-in-campaign/${id}`)}
              >
                Chi tiết
              </CustomButton>
            </div>
          )}
          <TextView
            label="Người quản lí chiến dịch"
            name="Manager"
            value={data?.data[0].manager + ""}
          />
          <TextView
            label="Địa điểm"
            name="Location"
            value={data?.data[0].location + ""}
          />

          <TextView
            label="Ngày bắt đầu"
            name="Start date"
            value={data?.data[0].startDate + ""}
          />
          <TextView
            label="Ngày kết thúc"
            name="End date"
            value={data?.data[0].endDate + ""}
          />
          <TextView
            label="Tiền quỹ ban đầu (VND)"
            name="id"
            value={numberWithCommas(data?.data[0].total || 0)}
          />
          <TextView
            label="Tổng số tiền đã chi (VND)"
            name="Total expense"
            value={numberWithCommas(data?.data[0].totalExpenses || 0)}
          />

          {role === "Admin" && (
            <TextView
              label="Số lượng thành viên"
              name="total member"
              value={data?.data[0].totalMember + ""}
            />
          )}
          <TextView
            label="Tình trạng"
            name="state"
            value={data?.data[0].state ? "Đang hoạt động" : "Đã hoàn thành"}
          />
        </div>

        <div className="mb-4">
          <TextAreaView
            label="Thông tin mô tả"
            name="Description"
            value={data?.data[0].description + ""}
          />
        </div>
      </div>
      <div className="grid grid-cols-4 space-x-4">
        {role === "Admin" && (
          <Button
            appearance="primary"
            onClick={() => history.push(`/donator-in-campaign/${id}`)}
          >
            Mạnh thường quân đã đóng góp
          </Button>
        )}
        {role === "Admin" && (
          <Button
            appearance="primary"
            onClick={() => history.push(`/members-in-campaign/${id}`)}
          >
            Thành viên đã tham gia
          </Button>
        )}
      </div>
    </div>
  );
};

export default CampaignInfo;
