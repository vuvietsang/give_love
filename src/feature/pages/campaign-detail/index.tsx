import Breadcrumbs, { BreadcrumbsItem } from "@atlaskit/breadcrumbs";
import PageHeader from "@atlaskit/page-header";
import React, { useState } from "react";
import { Link, useLocation, useParams } from "react-router-dom";
import CampaignInfo from "./components/CampaignInfo";
import ExpensesViewSection from "./components/ExpensesViewSection";
import useCampaignDetail from "./hooks/useCampaignDetail";
const CampaignDetail = () => {
  const { id } = useParams<{ id: string }>();

  const { data } = useCampaignDetail(id);
  return (
    <div>
      <PageHeader
        breadcrumbs={
          <Breadcrumbs>
            <BreadcrumbsItem
              href="/account"
              text="Danh sách thành viên"
              component={(props: any) => (
                <Link {...props} to="/account">
                  Danh sách chiến dịch
                </Link>
              )}
            />
            <BreadcrumbsItem text={data?.data[0].name || ""} />
          </Breadcrumbs>
        }
        actions={<ExpensesViewSection userId={id} />}
      >
        Chi tiết chiến dịch
      </PageHeader>
      <div className="space-y-3">
        <CampaignInfo />
        {/* <ListDepartment /> */}
      </div>
    </div>
  );
};

export default CampaignDetail;
