import { useAppDispatch } from "../../../../components/hooks/reduxHook";
import { login } from "../../../services/login";
import { LoginDto } from "../../../services/login/dto/login.dto";
import { login as loginAction } from "../../../slices/auth";
import { useMutation } from "react-query";
import { AxiosError } from "axios";
import { ErrorHttpResponse } from "../../../../interfaces/error_http_response.interface";
import { useHistory } from "react-router";
const useLogin = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();
  return useMutation(
    async (data: LoginDto) => {
      return await login(data);
    },
    {
      onSuccess: (data) => {
        dispatch(loginAction(data.data.token));
        //because data:any
        history.push("/dashboard");
      },
      onError: (_: AxiosError<ErrorHttpResponse>) => {},
    }
  );
};

export default useLogin;