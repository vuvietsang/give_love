import Spiner from "@atlaskit/spinner";
import { useAppSelector } from "@components/hooks/reduxHook";
import React, { useEffect, useState } from "react";
import { shallowEqual } from "react-redux";
import { ICampaign } from "src/interfaces/campaign.interface";
import useCampaignsForDonator from "../hooks/useCampaignsForDonator";
import "../styles.css";
import CampaignTable from "./CampaignTable";
import SearchSection from "./SearchSection";

interface SearchValue {
  name?: string;
  state?: string;
  pageNum: number;
  pageSize?: number;
  sort?: string;
}
const ListCampaignForDonator = () => {
  const userId = useAppSelector((state) => state.auth?.userId, shallowEqual);

  const { data, isLoading } = useCampaignsForDonator(userId);
  const [search, setSearch] = useState<SearchValue>({
    name: "",
    state: "true",
    pageNum: 0,
  });

  const [tmpData, setTmpData] = useState<ICampaign[]>(data?.data || []);

  useEffect(() => {
    const newData = getTmpData(data?.data || []);
    setTmpData(newData);
  }, [search, data]);

  const getTmpData = (data: ICampaign[]): ICampaign[] => {
    let finalData = data;
    if (data)
      finalData = data.filter((x) => {
        if (x.state === search.state && x.name.includes(search.name || "")) {
          return x;
        }
      });
    return finalData;
  };

  // @ts-ignore
  if (!data && !data?.data) {
    return <Spiner />;
  }

  return (
    <>
      <div>
        <SearchSection setSearch={setSearch} />
      </div>
      <CampaignTable autoPaging={true} data={tmpData} isLoading={isLoading} />
    </>
  );
};

export default ListCampaignForDonator;
