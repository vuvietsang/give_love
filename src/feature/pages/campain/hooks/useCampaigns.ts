import { useQuery } from "react-query";
import { getCampaigns } from "../../../services/campaign";

interface SearchValue {
  name?: string;
  state?: string;
  pageNum: number;
  pageSize?: number;
  sort?: string;
}
const useCampaigns = (filter?: SearchValue) => {
  return useQuery(["campaigns", filter], async () => {
    return await getCampaigns(filter);
  });
};

export default useCampaigns;
