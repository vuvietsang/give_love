import { useMutation, useQueryClient } from "react-query";

// interface reqData {
//   id: string;
//   data: updateAgencyDto;
// }
// const useUpdateAgency = () => {
//   const queryClient = useQueryClient();
//   return useMutation(
//     ["updateAgency"],
//     async ({ data, id }: reqData) => {
//       return await updateAgency(id, data);
//     },
//     {
//       onSuccess: (_, variable) => {
//         queryClient.invalidateQueries(["agency", variable.id]);
//       },
//     }
//   );
// };

// export default useUpdateAgency;
