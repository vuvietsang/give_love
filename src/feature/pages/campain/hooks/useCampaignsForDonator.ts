import { useQuery } from "react-query";
import { getCampaignsForDonator } from "../../../services/campaign";

const useCampaignsForDonator = (userId: string) => {
  return useQuery([`campaigns${userId}`], async () => {
    return await getCampaignsForDonator(userId);
  });
};

export default useCampaignsForDonator;
