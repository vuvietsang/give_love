import Button from "@atlaskit/button";
import PageHeader from "@atlaskit/page-header";
import React from "react";
import { useHistory } from "react-router-dom";
import ListAllDonators from "./components/ListAllDonators";
import { useParams } from "react-router";

const AddDonatorToCampaign = () => {
  const { id } = useParams<{ id: string }>();
  const history = useHistory();
  return (
    <div>
      <PageHeader
        actions={
          <Button
            onClick={() => {
              history.replace(`/donator-in-campaign/${id}`);
            }}
          >
            Trở về
          </Button>
        }
      >
        Các mạnh thường quân
      </PageHeader>

      <ListAllDonators />
    </div>
  );
};

export default AddDonatorToCampaign;
