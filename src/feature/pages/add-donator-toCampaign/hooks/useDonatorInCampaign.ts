import { useQuery } from "react-query";
import { getDonatorByCampaignId } from "../../../services/account";

const useDonatorInCampaign = (id: string) => {
  return useQuery([`donatorInCampaign${id}`, id], async () => {
    return getDonatorByCampaignId(id);
  });
};

export default useDonatorInCampaign;
