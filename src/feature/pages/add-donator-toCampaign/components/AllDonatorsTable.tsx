import DropdownMenu, {
  DropdownItem,
  DropdownItemGroup,
} from "@atlaskit/dropdown-menu";
import DynamicTable from "@atlaskit/dynamic-table";
import { RowType } from "@atlaskit/dynamic-table/dist/types/types";
import MoreIcon from "@atlaskit/icon/glyph/more";
import Spiner from "@atlaskit/spinner";
import TableEmptyView from "@components/TableEmptyView";
import { createKey } from "../../../../helpers/index";
import React from "react";
import { Link, useHistory } from "react-router-dom";
import { IUser } from "src/interfaces/user.interface";
import useAddDonator from "../hooks/useAddDonator";
import { useParams } from "react-router";
import AddIcon from "@atlaskit/icon/glyph/add";

const AllDonatorsTable = ({
  data,
  role,
  isLoading,
}: {
  data?: IUser[];
  role: string;
  isLoading: boolean;
}) => {
  const history = useHistory();
  if (!data || isLoading) {
    return <Spiner />;
  }
  const { id } = useParams<{ id: string }>();
  const { mutate: addDonator, error } = useAddDonator(id);
  const rowsForAdmin = data?.map<RowType>((account, index) => ({
    key: `row-${index}-${account.id}`,
    className: "border-b border-gray-200",
    cells: [
      {
        key: createKey(account.username),
        content: (
          <span className="flex items-center">
            <div>
              <Link to={`/account-details/${account.id}`} className="block">
                <p className="fullname">{account.username}</p>
              </Link>
            </div>
          </span>
        ),
      },
      {
        key: createKey(account.fullName),
        content: (
          <div>
            <span>{account.fullName}</span>
          </div>
        ),
      },
      {
        key: createKey(account.phone),
        content: (
          <div>
            <span>{account.phone}</span>
          </div>
        ),
      },
      {
        key: createKey(account.email),
        content: (
          <div>
            <span>{account.email}</span>
          </div>
        ),
      },

      // {
      //   content: (
      //     <div>
      //       <span>{roleMap.get(account.role)}</span>
      //     </div>
      //   ),
      // },
      {
        content: (
          <div className="flex justify-end pr-5">
            <DropdownMenu
              triggerButtonProps={{
                iconBefore: <AddIcon label="Thêm" />,
              }}
              triggerType="button"
            >
              <DropdownItemGroup>
                {/* <DropdownItem onClick={() => onDelete()}>
                        {t("common.action.delete")}
                      </DropdownItem> */}
                <DropdownItem
                  onClick={() => {
                    addDonator(
                      { campaignId: id, donatorId: account.id + "" },
                      {
                        onSuccess: (data) => {},
                      }
                    );
                  }}
                >
                  {"Thêm"}
                </DropdownItem>
              </DropdownItemGroup>
            </DropdownMenu>
          </div>
        ),
      },
    ],
  }));

  const rows = data?.map<RowType>((account, index) => ({
    key: `row-${index}-${account.id}`,
    className: "border-b border-gray-200",
    cells: [
      {
        key: createKey(account.username),
        content: (
          <span className="flex items-center">
            <div>
              <Link to={`/account-details/${account.id}`} className="block">
                <p className="fullname">{account.username}</p>
              </Link>
            </div>
          </span>
        ),
      },
      {
        key: createKey(account.fullName),
        content: (
          <div>
            <span>{account.fullName}</span>
          </div>
        ),
      },
      {
        key: createKey(account.phone),
        content: (
          <div>
            <span>{account.phone}</span>
          </div>
        ),
      },
      {
        key: createKey(account.email),
        content: (
          <div>
            <span>{account.email}</span>
          </div>
        ),
      },

      // {
      //   content: (
      //     <div>
      //       <span>{roleMap.get(account.role)}</span>
      //     </div>
      //   ),
      // },
    ],
  }));
  const createHead = (withWidth: boolean) => {
    if (role === "Admin")
      return {
        cells: [
          {
            key: "userName",
            content: "Tên đăng nhập",
            isSortable: true,
            width: withWidth ? 50 : undefined,
          },

          {
            key: "fullName",
            content: "Họ và Tên",
            isSortable: true,
            width: withWidth ? 50 : undefined,
          },
          {
            key: "phone",
            content: "Số điện thoại",
            // isSortable: true,
            width: withWidth ? 50 : undefined,
          },
          {
            key: "email",
            content: "Email",
            // isSortable: true,
            width: withWidth ? 50 : undefined,
          },

          {
            key: "more",
            shouldTruncate: true,
            width: withWidth ? 35 : undefined,
          },
        ],
      };
    return {
      cells: [
        {
          key: "userName",
          content: "Tên đăng nhập",
          isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        {
          key: "fullName",
          content: "Họ và Tên",
          isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        {
          key: "phone",
          content: "Số điện thoại",
          // isSortable: true,
          width: withWidth ? 50 : undefined,
        },
        {
          key: "email",
          content: "Email",
          // isSortable: true,
          width: withWidth ? 50 : undefined,
        },

        // {
        //   key: "role",
        //   content: "Vai trò",
        //   isSortable: true,
        //   width: withWidth ? 35 : undefined,
        // },
        // {
        //   key: "more",
        //   shouldTruncate: true,
        //   width: withWidth ? 35 : undefined,
        // },
      ],
    };
  };
  const head = createHead(true);
  return (
    <DynamicTable
      head={head}
      rows={role === "Admin" ? rowsForAdmin : rows}
      // rows={rows}
      loadingSpinnerSize="large"
      isLoading={isLoading}
      isFixedSize
      defaultSortKey="email"
      defaultSortOrder="ASC"
      rowsPerPage={10}
      onSort={() => console.log("onSort")}
      onSetPage={() => console.log("onSetPage")}
      emptyView={<TableEmptyView text="Không người dùng nào trong danh sách" />}
    />
  );
};

export default AllDonatorsTable;
