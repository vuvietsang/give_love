import { useMutation, useQuery, useQueryClient } from "react-query";
import { createAccount, updateAccount } from "../../../services/account";
import { CreateAccountDto } from "src/feature/services/account/dto/create-account-dto";
import { UpdateAccountDto } from "src/feature/services/account/dto/update-account-dto";

const useUpdateAccount = () => {
  const queryClient = useQueryClient();
  return useMutation(
    async (data: UpdateAccountDto) => {
      return await updateAccount(data);
    },
    {
      onSettled: () => {
        queryClient.invalidateQueries("accounts");
        queryClient.invalidateQueries("account");
      },
    }
  );
};

export default useUpdateAccount;
