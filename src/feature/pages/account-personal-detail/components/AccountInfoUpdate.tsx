import Button from "@atlaskit/button/loading-button";
import { DatePicker } from "@atlaskit/datetime-picker";
import Form, { FormFooter } from "@atlaskit/form";
import DateGetter from "@components/formInputs/DatePicker";
import InputField from "@components/formInputs/InputField";
import TextView from "@components/formInputs/TextView";
import { useAppSelector } from "@components/hooks/reduxHook";
import { yupResolver } from "@hookform/resolvers/yup";
import React, { useEffect } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { shallowEqual } from "react-redux";
import { useParams } from "react-router";
import * as yup from "yup";
import useAccountById from "../hooks/useAccountById";
import useUpdateAccount from "../hooks/useUpdateAccount";
interface FormValues {
  username: string;
  fullName: string;
  email: string;
  phone: string;
  address: string;
  dob: string;
}
const AccountInfoUpdate = ({
  edit = false,
  setEdit,
}: {
  edit?: boolean;
  setEdit: (data: boolean) => void;
}) => {
  const { mutate: update, isLoading: updateLoading } = useUpdateAccount();

  const { id } = useParams<{ id: string }>();
  const { data, isLoading } = useAccountById(id);

  const schema = yup.object().shape({
    email: yup
      .string()
      .typeError("Không đúng định dạng")
      .required("Email không được để trống")
      .email("Không đúng định dạng"),
    fullName: yup
      .string()
      .typeError("Không đúng định dạng")
      .min(3, "Cần ít nhất 3 ký tự"),
    phone: yup
      .number()
      .typeError("Không đúng định dạng")
      .required("Tên đăng nhập không được để trống"),
    address: yup
      .string()
      .typeError("Không đúng định dạng")
      .min(3, "Cần ít nhất 3 ký tự"),
    dob: yup
      .string()
      .typeError("Không đúng định dạng")
      .min(3, "Cần ít nhất 3 ký tự"),
  });

  const RHFformProps = useForm<FormValues>({
    defaultValues: {
      email: data ? data.email : "",
      fullName: data ? data.fullName : "",
      phone: data ? data.phone : "",
      address: data ? data.address : "",
      dob: data ? data.dob : "",
    },
    resolver: yupResolver(schema),
  });
  const { handleSubmit } = RHFformProps;
  useEffect(() => {
    if (isLoading) return;
    RHFformProps.reset({
      email: data ? data.email : "",
      fullName: data ? data.fullName : "",
      phone: data ? data.phone : "",
      address: data ? data.address : "",
      dob: data ? data.dob : "",
    });

    return () => {};
  }, [data, isLoading, RHFformProps]);
  const onCancel = () => {
    setEdit(false);
    RHFformProps.reset({
      email: data ? data.email : "",
      fullName: data ? data.fullName : "",
      phone: data ? data.phone : "",
      address: data ? data.address : "",
      dob: data ? data.dob : "",
    });
    RHFformProps.clearErrors();
  };
  return (
    <Form onSubmit={() => {}} isDisabled={!edit || updateLoading}>
      {({ formProps, dirty, submitting }) => (
        <FormProvider {...RHFformProps}>
          <form
            {...formProps}
            onSubmit={handleSubmit((data) => {
              update(
                { ...data, userId: id },
                {
                  onSuccess: () => {
                    setEdit(false);
                  },
                }
              );
            })}
          >
            <div className="grid grid-cols-2 gap-x-4">
              <TextView
                value={!data ? "" : data.username}
                label="Tên đăng nhập"
                name="username"
                disabled
              />
              {edit ? (
                <>
                  <InputField
                    form={RHFformProps}
                    label="Họ và tên"
                    name="fullName"
                  />
                  <InputField
                    form={RHFformProps}
                    label="Số điện thoại"
                    name="phone"
                  />

                  <InputField form={RHFformProps} label="Email" name="email" />
                  <InputField
                    form={RHFformProps}
                    label="Địa chỉ"
                    name="address"
                  />

                  <DateGetter
                    label="Ngày sinh"
                    form={RHFformProps}
                    name="dob"
                  />
                </>
              ) : (
                <>
                  <InputField
                    form={RHFformProps}
                    label="Họ và tên"
                    name="fullName"
                    disabled
                  />
                  <InputField
                    form={RHFformProps}
                    label="Số điện thoại"
                    name="phone"
                    disabled
                  />

                  <InputField
                    form={RHFformProps}
                    label="Email"
                    name="email"
                    disabled
                  />
                  <InputField
                    form={RHFformProps}
                    label="Địa chỉ"
                    name="address"
                    disabled
                  />
                  <InputField
                    form={RHFformProps}
                    label="Ngày sinh"
                    name="dob"
                    disabled
                  />
                </>
              )}
            </div>

            {edit && (
              <FormFooter>
                <div className="space-x-2">
                  <Button
                    type="button"
                    appearance="danger"
                    isLoading={updateLoading}
                    onClick={onCancel}
                  >
                    Hủy
                  </Button>
                  <Button
                    type="submit"
                    appearance="primary"
                    isLoading={updateLoading}
                  >
                    Đồng ý
                  </Button>
                </div>
              </FormFooter>
            )}
          </form>
        </FormProvider>
      )}
    </Form>
  );
};

export default AccountInfoUpdate;
