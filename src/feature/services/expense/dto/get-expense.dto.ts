import { IExpense } from "../../../../interfaces/expense.interface";
import { ISuccessHttpResponse } from "../../../../interfaces/success_http_response.interface";
export interface GetExpenseResponse extends ISuccessHttpResponse {
  data: IExpense[];
}
