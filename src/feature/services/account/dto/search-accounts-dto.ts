import { ISuccessHttpResponse } from "../../../../interfaces/success_http_response.interface";
import { IUser } from "../../../../interfaces/user.interface";

export interface SearchAccountDto {
  email?: string;
  phone?: string;
  username?: string;
  fullName?: string;
  role?: string;
}
