import { ISuccessHttpResponse } from "../../../../interfaces/success_http_response.interface";
import { IUser } from "../../../../interfaces/user.interface";

export interface GetAccountsResponse extends ISuccessHttpResponse {
  data: { data: IUser[]; totalPage: number };
}

export interface GetAccountResponse extends ISuccessHttpResponse {
  data: IUser;
}

export interface GetAccountsByCampaignIdResponse extends ISuccessHttpResponse {
  data: IUser[];
}
