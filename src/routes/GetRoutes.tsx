import { Main } from "@atlaskit/page-layout";
import React, { memo, Suspense } from "react";
import { Switch } from "react-router-dom";
import { useAppSelector } from "../components/hooks/reduxHook";
import ProtectedRoute from "../components/ProtectedRoute";
import routes from "../feature/routes/index";

function GetRoutes() {
  return (
    <Main>
      <div className="w-full h-full px-10">
        <Suspense
          fallback={
            <div id="loader-wrapper">
              <div id="loader"></div>
            </div>
          }
        >
          <Switch>
            {routes.map((route, indx) => {
              return <ProtectedRoute key={route.path} {...route} />;
            })}
          </Switch>
        </Suspense>
      </div>
    </Main>
  );
}

export default memo(GetRoutes);
