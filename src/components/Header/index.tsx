import {
  AtlassianNavigation,
  CustomProductHome,
  Notifications,
  Profile,
} from "@atlaskit/atlassian-navigation";
import Avatar from "@atlaskit/avatar";
import DropdownMenu, {
  DropdownItem,
  DropdownItemGroup,
} from "@atlaskit/dropdown-menu";
import { NotificationIndicator } from "@atlaskit/notification-indicator";
import { TopNavigation } from "@atlaskit/page-layout";
import React from "react";
import { shallowEqual } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
// import LogoIcon from "@assets/ukbase_white.png";
import LogoIcon from "../../assets/logo/love_logo.png";
import { logout } from "../../feature/slices/auth";
import { useAppDispatch, useAppSelector } from "../hooks/reduxHook";
import SearchSection from "../../feature/pages/account/components/SearchSection";

const NotificationsBadge = () => (
  <NotificationIndicator
    onCountUpdated={console.log}
    // notificationLogProvider={Promise.resolve({}) as any}
  />
);
// const theme = generateTheme({
//   name: "high-contrast",
//   backgroundColor: "#fff",
//   highlightColor: "#fafafa",
// });

const Header = () => {
  const history = useHistory();
  const isAuth = useAppSelector((state) => state.auth.isAuth, shallowEqual);
  const userId = useAppSelector((state) => state.auth?.userId, shallowEqual);
  const dispatch = useAppDispatch();
  return isAuth ? (
    <TopNavigation testId="top-navigation">
      {isAuth && (
        <AtlassianNavigation
          // theme={theme}
          label="site"
          primaryItems={[]}
          // renderAppSwitcher={() => <AppSwitcher />}
          renderProductHome={() => (
            <Link to="#">
              <CustomProductHome
                iconUrl={LogoIcon}
                iconAlt="UKBase"
                logoUrl={LogoIcon}
                logoAlt="UKBase logo"
                logoMaxWidth={60}
                // logo={() => <div className="text-lg font-medium">UKBase</div>}
              />
            </Link>
          )}
          // renderSearch={()=>(
          //   <SearchSection/>
          // )}
          renderProfile={() => (
            <DropdownMenu
              position="bottom right"
              trigger={
                <div className="cursor-pointer">
                  <Profile
                    icon={
                      <Avatar
                        appearance="circle"
                        src="https://pbs.twimg.com/profile_images/803832195970433027/aaoG6PJI_400x400.jpg"
                        size="medium"
                        name="John Doe"
                      />
                    }
                    tooltip="name"
                  />
                </div>
              }
            >
              <DropdownItemGroup>
                <DropdownItem
                  onClick={() =>
                    history.push(`/account-personal-details/${userId}`)
                  }
                >
                  Thông tin tài khoản
                </DropdownItem>
                <DropdownItem onClick={() => dispatch(logout())}>
                  Đăng xuất
                </DropdownItem>
              </DropdownItemGroup>
            </DropdownMenu>
          )}
          renderNotifications={() => (
            <Notifications badge={NotificationsBadge} tooltip="Notifications" />
          )}
        />
      )}
    </TopNavigation>
  ) : null;
};

export default Header;
