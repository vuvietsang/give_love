import { N500 } from "@atlaskit/theme/colors";
import React from "react";
import { FC } from "react";
import IconWrapper from "./IconWrapper";

const SidebarOverrideComponent: FC = ({ children }) => {
  const [flag] = React.useState(false);
  return (
    <div
      style={{
        alignItems: "center",
        boxSizing: "border-box",
        color: N500,
        display: "flex",
        flexShrink: 0,
        flexDirection: "column",
        height: "100vh",
        paddingBottom: 16,
        paddingTop: 24,
        width: 64,
      }}
    >
      {children}
      <IconWrapper
        onClick={() => console.log("onNewButtonClicked")}
        flag={flag}
      ></IconWrapper>
    </div>
  );
};
export default SidebarOverrideComponent;
