import { B50, N30A } from '@atlaskit/theme/colors';
import { FC } from 'react';
import React from 'react';
interface IconWrapperProps {
  onClick: () => void;
  flag: boolean;
}
const IconWrapper: FC<IconWrapperProps> = ({ onClick, flag, ...props }) => (
  <button
    type='button'
    css={{
      alignItems: 'center',
      background: 0,
      border: 0,
      borderRadius: '50%',
      color: 'inherit',
      cursor: flag ? 'pointer' : undefined,
      display: 'flex',
      fontSize: 'inherit',
      height: '40px',
      justifyContent: 'center',
      lineHeight: 1,
      marginBottom: '16px',
      padding: 0,
      width: '40px',
      '&:hover': {
        backgroundColor: flag ? N30A : undefined,
      },
      '&:active': {
        backgroundColor: flag ? B50 : undefined,
        outline: 0,
      },
    }}
    {...props}
  />
);

export default IconWrapper;
