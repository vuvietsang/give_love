import React from "react";
import {
  NavigationHeader,
  Header,
  SideNavigation,
} from "@atlaskit/side-navigation";
import { AtlassianIcon } from "@atlaskit/logo";
export default function SidebarContent() {
  return (
    <React.Fragment>
      <SideNavigation label="project" testId="side-navigation">
        <NavigationHeader>
          <Header iconBefore={<AtlassianIcon />} description="Campain">
            Campaign created
          </Header>
        </NavigationHeader>
      </SideNavigation>
    </React.Fragment>
  );
}
