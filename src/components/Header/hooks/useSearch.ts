import { useAppDispatch, useAppSelector } from "@components/hooks/reduxHook";
import { useMutation, useQueryClient } from "react-query";
import { shallowEqual } from "react-redux";
import { SearchAccountDto } from "src/feature/services/account/dto/search-accounts-dto";
import { setValue } from "../../../feature/slices/search";

const useSearch = () => {
  const dispatch = useAppDispatch();
  const queryClient = useQueryClient();
  const searchValue = useAppSelector(
    (state) => state.searchValue,
    shallowEqual
  );
  let key = "";

  return dispatch(setValue(data));

  // return useMutation(
  //   async (data: SearchAccountDto) => {
  //     key = data.username;
  //     return dispatch(setValue(data));
  //   },
  //   {
  //     onSettled: () => {
  //       queryClient.invalidateQueries("accounts-search");
  //     },
  //   }
  // );
};

export default useSearch;
