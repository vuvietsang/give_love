import React from "react";

const TableEmptyView = ({ text }: { text: string }) => {
  return <div>{text}</div>;
};

export default TableEmptyView;
