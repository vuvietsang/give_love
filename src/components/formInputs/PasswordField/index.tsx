import { ErrorMessage, Field } from "@atlaskit/form";
import Textfield from "@atlaskit/textfield";
import React, { FC } from "react";
import { Controller, UseFormReturn } from "react-hook-form";

interface Props {
  form: UseFormReturn<any>;
  name: string;
  label: string;
  disabled?: boolean;
}

const PasswordField: FC<Props> = (props: Props) => {
  const { form, name, label, disabled } = props;
  const { formState } = form;
  const hasError = formState.errors[name];

  return (
    <Field label={label} name={name}>
      {() => (
        <Controller
          name={name}
          control={form.control}
          render={({ field }) => (
            <>
              <Textfield
                type="password"
                {...field}
                isInvalid={hasError}
                id={name}
              />
              {hasError && (
                <ErrorMessage>{formState.errors[name]?.message}</ErrorMessage>
              )}
            </>
          )}
        />
      )}
    </Field>
  );
};

export default PasswordField;
