import React, { FC } from "react";
import PropTypes from "prop-types";
import { Controller, UseFormReturn } from "react-hook-form";
import Select from "@atlaskit/select";
import { Field } from "@atlaskit/form";

interface Props {
  form: UseFormReturn<any>;
  name: string;
  label: string;
  disabled?: boolean;
  placeholder?: string;
  data: { key: string; value: string }[];
  isCompact?: boolean;
}

const SelectField: FC<Props> = (props: Props) => {
  const { form, name, data, label, isCompact, ...rest } = props;
  const { formState } = form;
  const hasError = formState.errors[name];
  const bindedData = data.map(({ key, value }) => ({
    label: key,
    value: value,
  }));

  return (
    <Field label={label} name={name}>
      {() => (
        <Controller
          name={name}
          control={form.control}
          rules={{ required: true }}
          render={({ field }) => (
            <>
              <Select
                spacing={"compact"}
                inputId="single-select-example"
                className="single-select"
                classNamePrefix="react-select"
                options={bindedData}
                {...field}
              />
            </>
          )}
        />
      )}
    </Field>
  );
};

export default SelectField;
