import { ErrorMessage, Field } from "@atlaskit/form";
import TextArea from "@atlaskit/textarea";
import React, { FC, Fragment } from "react";
import { Controller, UseFormReturn } from "react-hook-form";
import { css } from "@emotion/core";

interface Props {
  name: string;
  label: string;
  disabled?: boolean;
  value: string;
}

const bigFontStyles = css({
  // container style
  padding: 10,
  "& > [data-ds--text-field--container]": {
    // input style
    fontSize: 30,
  },
});
const TextView: FC<Props> = ({ name, label, value }: Props) => {
  return (
    <Field label={label} name={name}>
      {() => (
        <TextArea
          resize="auto"
          maxHeight="20vh"
          minimumRows={4}
          value={value}
          css={bigFontStyles}
          isReadOnly={true}
        />
      )}
    </Field>
  );
};
export default TextView;
