import { ErrorMessage, Field, HelperMessage } from "@atlaskit/form";
import Textfield from "@atlaskit/textfield";
import React, { FC, Fragment } from "react";
import { Controller, UseFormReturn } from "react-hook-form";
import { DatePicker } from "@atlaskit/datetime-picker";

interface Props {
  form: UseFormReturn<any>;
  name: string;
  label?: string;
  disabled?: boolean;
  placeholder?: string;
}
const DateGetter: FC<Props> = ({
  form,
  name,
  label,
  disabled,
  placeholder,
}: Props) => {
  const { formState } = form;
  const hasError = formState.errors[name] && formState.touchedFields[name];
  return (
    <Field label={label} name={name}>
      {() => (
        <Controller
          name={name}
          control={form.control}
          render={({ field }) => (
            <Fragment>
              <>
                <DatePicker {...field} placeholder="mm/dd/yyyy" />
                {/* <HelperMessage>
                  Help or instruction text goes here
                </HelperMessage> */}
              </>
              {hasError && (
                <ErrorMessage>{formState.errors[name]?.message}</ErrorMessage>
              )}
            </Fragment>
          )}
        />
      )}
    </Field>
  );
};
export default DateGetter;
