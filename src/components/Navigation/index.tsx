import { LeftSidebar } from "@atlaskit/page-layout";
import { useAppSelector } from "@components/hooks/reduxHook";
import React from "react";
import AppSideNavigation from "./AppSideNavigation";

const Navigation = () => {
  const isAuth = useAppSelector((state) => state?.auth?.isAuth);
  return isAuth ? (
    <LeftSidebar isFixed={true} width={250} shouldPersistWidth={true}>
      <AppSideNavigation />
    </LeftSidebar>
  ) : null;
};

export default Navigation;
