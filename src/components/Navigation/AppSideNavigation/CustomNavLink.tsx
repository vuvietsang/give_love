import React from "react";
import { Link, useRouteMatch } from "react-router-dom";
interface Props {
  to: string;
  exact?: boolean;
  render: (match: boolean) => React.ReactElement;
}
const CustomNavLink = ({ exact = true, to, render }: Props) => {
  const match = useRouteMatch({
    path: to,
    exact: exact,
  });
  return (
    <Link to={to} className="hover:no-underline">
      {render(!!match)}
    </Link>
  );
};

export default CustomNavLink;
