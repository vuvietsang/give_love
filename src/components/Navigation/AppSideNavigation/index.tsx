import PeopleGroupIcon from "@atlaskit/icon/glyph/people-group";
import ActivityIcon from "@atlaskit/icon/glyph/activity";
import {
  ButtonItem,
  Footer,
  Header,
  NavigationFooter,
  NavigationHeader,
  NestableNavigationContent,
  Section,
  SideNavigation,
} from "@atlaskit/side-navigation";
import React from "react";
import CustomNavLink from "./CustomNavLink";
import RocketIcon from "./RocketIcon";
import { useAppSelector } from "@components/hooks/reduxHook";
import { shallowEqual } from "react-redux";

const AppSideNavigation = () => {
  const role = useAppSelector((state) => state.auth?.role, shallowEqual);
  return (
    <SideNavigation label="project">
      <NavigationHeader>
        <Header
          // component={InteractiveContainer}
          iconBefore={<RocketIcon />}
          description={
            role === "Admin" ? "Quản lý Give Love" : "Mạnh thường quân"
          }
        >
          Tài khoản
        </Header>
      </NavigationHeader>
      <NestableNavigationContent>
        <Section>
          <CustomNavLink
            to="/dashboard"
            render={(match) => (
              <ButtonItem
                iconBefore={<ActivityIcon label="campains" />}
                isSelected={match}
              >
                {role === "Admin"
                  ? "Quản lí chiến dịch"
                  : "Danh sách chiến dịch"}
              </ButtonItem>
            )}
          />
          {role === "Admin" && (
            <CustomNavLink
              to="/account"
              render={(match) => (
                <ButtonItem
                  iconBefore={<PeopleGroupIcon label="accounts" />}
                  isSelected={match}
                >
                  Quản lí tài Khoản
                </ButtonItem>
              )}
            />
          )}
        </Section>
      </NestableNavigationContent>
      <NavigationFooter>
        <Footer>You're in a next-gen project</Footer>
      </NavigationFooter>
    </SideNavigation>
  );
};

export default AppSideNavigation;
