import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";
import GetRoutes from "./routes/GetRoutes";
import Navigation from "./components/Navigation";
import { Content, PageLayout } from "@atlaskit/page-layout";
import { QueryClient, QueryClientProvider } from "react-query";
import Header from "../src/components/Header";
const queryClient = new QueryClient();
function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <PageLayout>
        <Header />
        <Content testId="content">
          <Navigation />
          <GetRoutes />
        </Content>
      </PageLayout>
    </QueryClientProvider>
  );
}

export default App;
